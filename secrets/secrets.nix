let
  # user1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL0idNvgGiucWgup/mP78zyC23uFjYq0evcWdjGQUaBH";
  # user2 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILI6jSq53F/3hEmSs+oq9L4TwOo1PrDMAgcA1uo1CCV/";
  # users = [ user1 user2 ];
  nomada-generic = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICDcVEo3uZSx5UnE/+Vy1gC9EH9ncXffz0aPaONKtNo9";
  nomada-t440s = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC9G7NXmDgi34UnEUalM8fpd6cKw23YEkuk5gIguBkEC";
  nomada-coder = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICKuyP9Ca868lPalQ+g+9hnGi5NA0SCPmT2tuAuL90jB";
  # system2 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKzxQgondgEYcLpcPdJLrTdNgZ2gznOHCAxMdaceTUT1";
  # systems = [ system1 system2 ];
in {
  # "secret1.age".publicKeys = [nomada-generic nomada-coder];
  "t440s.age".publicKeys = [nomada-generic nomada-coder nomada-t440s];
  # "secret2.age".publicKeys = users ++ systems;
}
