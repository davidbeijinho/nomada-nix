{pkgs, ...}: {
  virtualisation.containers.enable = true;
  virtualisation = {
    podman = {
      enable = true;

      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;

      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };
    # docker.enable = true;
  };
  # users.users.nomada.extraGroups = [ "docker" ];

  environment.systemPackages = with pkgs; [
    podman-compose
    podman
    podman-desktop
    # docker-compose
  ];
  # /etc/containers/containers.conf

  virtualisation.containers.containersConf.settings = {
    containers = {
      default_ulimits = [
        "nofile=65535:65535"
      ];
    };
  };

  security.pam.loginLimits = [
    {
      domain = "*";
      item = "nofile";
      type = "-";
      value = "65535";
    }
    # { domain = "*"; item = "memlock"; type = "-"; value = "32768"; }
  ];

  #/etc/containers/registries.conf
  # virtualisation.containers.registries.search =[
  #    "docker.io"
  # ];
}
