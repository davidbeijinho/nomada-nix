{
  pkgs,
  inputs,
  ...
}: {
  networking.firewall.allowedTCPPorts = [
    6443 # k3s: required so that pods can reach the API server (running on port 6443 by default)
    # 2379 # k3s, etcd clients: required if using a "High Availability Embedded etcd" configuration
    # 2380 # k3s, etcd peers: required if using a "High Availability Embedded etcd" configuration
  ];
  networking.firewall.allowedUDPPorts = [
    # 8472 # k3s, flannel: required if using multi-node for inter-node networking
  ];

  # first we need to disable the service module coming from nixpkgs
  # the path we give here is relative to the nixos/modules/ dir
  disabledModules = [
    "services/cluster/k3s/default.nix"
  ];

  # then we add an import of the same module from nixpkgs-unstable
  imports = [
    "${inputs.nixpkgs-unstable}/nixos/modules/services/cluster/k3s/default.nix"
  ];

  services.k3s = {
    serverAddr = "https://192.168.1.113:6443";
    enable = true;
    role = "server";
    extraFlags = toString [
      # "--debug" # Optionally add additional args to k3s
      "--disable traefik"
      "--write-kubeconfig-mode=644"
    ];
  };

  environment.systemPackages = with pkgs; [
    kubectl
    k9s
  ];
}
