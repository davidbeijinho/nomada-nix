{
  pkgs,
  inputs,
  ...
}: let
  repo = pkgs.fetchgit {
    url = "https://gitlab.com/davidbeijinho/infra-nomada.git";
    rev = "eae68ac758c4bf115276484f692fc030ed291bf4";
    sha256 = "0qj40z81zipxyvca4pgpbclzvf1z3m62r52mlblfrdrgnqwhvhq3";
  };
  # nix-prefetch-git --rev your-commit-hash https://github.com/your-username/your-repo.git
  # nix-prefetch-git https://gitlab.com/davidbeijinho/infra-nomada.git
  # {
  #   "url": "https://gitlab.com/davidbeijinho/infra-nomada.git",
  #   "rev": "eae68ac758c4bf115276484f692fc030ed291bf4",
  #   "date": "2024-10-25T11:27:07+02:00",
  #   "path": "/nix/store/5k09n38325n87frypzpqx5qlqap263px-infra-nomada",
  #   "sha256": "0qj40z81zipxyvca4pgpbclzvf1z3m62r52mlblfrdrgnqwhvhq3",
  #   "hash": "sha256-A8MNObYvt+zoolWULEwdP7j9KVv3XaLY9v3GH9AHRGI=",
  #   "fetchLFS": false,
  #   "fetchSubmodules": false,
  #   "deepClone": false,
  #   "leaveDotGit": false
  # }
in {
  services.k3s.manifests = {
    # copied to /var/lib/rancher/k3s/server/manifests
    argocd-1-namespace.source = "${repo}/nomada-centre/resources/argo-cd/namespace.yaml";
    argocd-2-helm.source = "${repo}/nomada-centre/resources/argo-cd/resources-helm.yaml";
    argocd-3-project.source = "${repo}/nomada-centre/resources/gitops/resources/project-gitops.yaml";
    argocd-4-app.source = "${repo}/nomada-centre/resources/gitops/gitops-app.yaml";
  };
}
