{inputs, ...}: {
  # https://nixos.wiki/wiki/Automatic_system_upgrades
  system.autoUpgrade = {
    enable = true;
    flake = inputs.self.outPath;
    flags = [
      "--update-input"
      "nixpkgs"
      "-L" # print build logs
    ];
    dates = "weekly";
    # dates = "02:00";
    # randomizedDelaySec = "45min";
  };

  # To see the status of the timer run
  #   systemctl status nixos-upgrade.timer
  # The upgrade log can be printed with this command
  #   systemctl status nixos-upgrade.service

  #
  nix.settings.auto-optimise-store = true;
  #
  nix.optimise = {
    automatic = true;
    dates = ["weekly"];
  };

  # https://nixos.wiki/wiki/Storage_optimization
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };
}
