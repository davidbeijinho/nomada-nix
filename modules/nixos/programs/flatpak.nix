{pkgs, ...}: {
  # xdg.portal.enable = true;
  services.flatpak.enable = true;
  services.flatpak.packages = [
    #   "com.uploadedlobster.peek"
    #   "de.manuel_kehl.go-for-it"
    #   "org.sqlitebrowser.sqlitebrowser"
    #   "org.gnome.Calculator"
    #   # { appId = "com.brave.Browser"; origin = "flathub";  }
    #   # "com.obsproject.Studio"
    #   # "im.riot.Riot"
    #   # flatpak_list:

    #   # "io.podman_desktop.PodmanDesktop"
    #   "org.mozilla.Thunderbird"
    #   # - me.kozec.syncthingtk
    #   # "com.github.zocker_160.SyncThingy"
    #   "org.videolan.VLC"
    #   "org.chromium.Chromium"
    #   #- org.mozilla.firefox # install native package
    #   #- com.vscodium.codium # install native package
    #   #- com.visualstudio.code
    #   #- com.visualstudio.code-oss
    #   #- flathub org.gnu.emacs
    #   # - com.logseq.Logseq
    #   "com.github.tchx84.Flatseal"

    #   # flatpak_list_personal:
    #   "com.ultimaker.cura"
    "com.prusa3d.PrusaSlicer"
    "com.github.tchx84.Flatseal"
    #   # - com.ktechpit.ultimate-media-downloader
    #   #- io.webtorrent.WebTorrent
  ];
  environment.systemPackages = with pkgs; [
    flatpak
  ];

  environment.shellInit = "export XDG_DATA_DIRS=$XDG_DATA_DIRS:/usr/share:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share";
}
