{
  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
    ohMyZsh.enable = true;
    # enableCompletion = true;
    # autosuggestion.enable = true;
  };
}
