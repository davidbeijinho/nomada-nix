{pkgs, ...}: {
  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  services.xserver = {
    # Enable the X11 windowing system.
    enable = true;

    windowManager.i3.enable = true;

    displayManager = {
      lightdm.enable = false;
      gdm.enable = false;
    };

    desktopManager.xfce = {
      enable = true;

      noDesktop = true;
      enableXfwm = false;
      enableScreensaver = false;
    };

    # Configure keymap in X11
    xkb.layout = "us";
    xkb.variant = "";

    excludePackages = [
      pkgs.xterm
    ];
  };

  services.displayManager = {
    sddm.enable = true;
    defaultSession = "xfce+i3";
  };

  services.xscreensaver.enable = true;

  environment.systemPackages = with pkgs; [
    xfce.xfce4-panel
    xfce.xfce4-notifyd
    xfce.xfce4-panel-profiles
    xfce.xfce4-clipman-plugin
    xfce.xfce4-whiskermenu-plugin
    xfce.xfce4-pulseaudio-plugin
    xfce.xfce4-netload-plugin
    xfce.xfce4-i3-workspaces-plugin
    rofi
    pavucontrol
    feh
    xarchiver
    xscreensaver
  ];

  programs.thunar.plugins = with pkgs.xfce; [
    thunar-archive-plugin
    thunar-volman
  ];
}
