{pkgs, ...}: {
  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  services.xserver = {
    # Enable the X11 windowing system.
    enable = true;

    # windowManager.i3.enable = true;

    # displayManager = {
    #   lightdm.enable = false;
    #   gdm.enable = false;
    # };

    # desktopManager.xfce = {
    #   enable = true;

    #   noDesktop = true;
    #   enableXfwm = false;
    # };

    # Configure keymap in X11
    xkb.layout = "us";
    xkb.variant = "";

    displayManager.startx.enable = true;
    # displayManager.lightdm.enable = true;
    # logFile = "/var/log/Xorg.0.log";
  };
  # services.greetd.enable = true;

  # services.greetd.enable = true;
  services.greetd = {
    enable = true;
    settings = {
      default_session = {
        #   # command = "${pkgs.sway}/bin/sway --config ${swayConfig}";
        #   # command = "${pkgs.xorg.xinit}/bin/startx";
        command = "${pkgs.greetd.greetd}/bin/agreety --cmd startx";
      };
      initial_session = {
        # command = "${pkgs.greetd.greetd}/bin/agreety --cmd startx";
        command = "${pkgs.xorg.xinit}/bin/startx";
        user = "nomada";
      };
    };
  };

  # services.getty.autologinUser = "nomada";

  # services.displayManager = {
  #   enable = true;
  #   sddm.enable = true;
  #   # defaultSession = "xfce+i3";
  #   # execCmd = "startx";
  #   autoLogin = {
  #     enable = true;
  #     user = "nomada";
  #   };
  # };

  # services.xserver.excludePackages = [
  #   pkgs.xterm
  # ];

  environment.systemPackages = with pkgs; [
    # xfce.xfce4-panel
    # xfce.xfce4-notifyd
    # xfce.xfce4-panel-profiles
    # xfce.xfce4-clipman-plugin
    # xfce.xfce4-whiskermenu-plugin
    # xfce.xfce4-pulseaudio-plugin
    # xfce.xfce4-netload-plugin
    # xfce.xfce4-i3-workspaces-plugin
    # rofi
    # pavucontrol
    # feh
    xscreensaver
  ];

  # programs.thunar.plugins = with pkgs.xfce; [
  #   thunar-archive-plugin
  #   thunar-volman
  # ];
}
