{pkgs, ...}: {
  fonts = {
    packages = with pkgs; [
      fira-code
      meslo-lgs-nf
      fira-code-symbols
    ];
  };
}
