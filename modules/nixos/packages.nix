{pkgs, ...}: {
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vscodium
    devbox
    #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #  wget
    python3 # for dotfiles
    firefox
    firefox-devedition-bin
    # git
    htop
    xarchiver
    # bash # not sure if needed
    chromium
    thunderbird
    # removed
    # zed-editor
    kdePackages.kdeconnect-kde
  ];
  programs.kdeconnect.enable = true;
}
