let
  # secrets = import ../../secrets/nomada-secrets.nix;
  # hostName = "nomada-t440s";
in {
  # networking.hostName = hostName; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # networking.wireless.networks."hive2g" = {
  # };
  # TODO fix this
  # networking.wireless.networks = {
  #   sagemcomA910 = {
  #     psk = secrets.homeWifi.psk;
  #     priority = 2;
  #   };

  #   sagemcomA910-5G = {
  #     psk = secrets.homeWifi.psk;
  #     priority = 3;
  #   };

  #   hive2g = {
  #     psk = secrets.labWifi.psk;
  #     priority = 4;
  #   };

  #   hive5g = {
  #     psk = secrets.labWifi.psk;
  #   };
  # };
}
