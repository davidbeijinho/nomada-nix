{pkgs, ...}: let
  # secrets = import ../../../secrets/nomada-secrets.nix;
in {
  # TODO: Configure your system-wide user settings (groups, etc), add more users as needed.
  # users.mutableUsers = true;
  users.users = {
    # FIXME: Replace with your username
    nomada = {
      # TODO: You can set an initial password for your user.
      # If you do, you can skip setting a root password by passing '--no-root-passwd' to nixos-install.
      # Be sure to change it (using passwd) after rebooting!
      isNormalUser = true;
      description = "nomada";
      extraGroups = ["networkmanager" "wheel"];
      initialPassword = "banana";
      # TODO fix this
      # password = secrets.password;
      packages = with pkgs; [
        # thunderbird
      ];
      # shell = pkgs.zsh;
      # useDefaultShell = true;
      # config.age.secrets.emojiedDBPassword.path;
      # openssh.authorizedKeys.keys = [
      #   # programs.ssh.userKnownHostsFile

      #   # TODO: Add your SSH public key(s) here, if you plan on using SSH to connect
      # ];
      # services.openssh.hostKeys = [
      # {
      #   bits = 4096;
      #   path = "/etc/ssh/ssh_host_rsa_key";
      #   type = "rsa";
      # }
      # {
      #   path = "/etc/ssh/ssh_host_ed25519_key";
      #   type = "ed25519";
      # }
      #];
      # TODO: Be sure to add any other groups you need (such as networkmanager, audio, docker, etc)
    };
  };

  programs.ssh.knownHosts = {
    "gitlab.com" = {
      publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf";
    };
    "github.com" = {
      publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl";
    };
  };
}
