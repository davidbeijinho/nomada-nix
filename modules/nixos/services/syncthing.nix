{
  config,
  lib,
  pkgs,
  ...
}: let
  secrets = import ../../../secrets/nomada-secrets.nix;
  # hostName = "nomada-t440s";
in {
  # with lib; let
  #   cfg = config.services.syncthing;
  # in {
  #   options.services.syncthing = {
  #     enable = mkOption {
  #       type = types.bool;
  #       default = false;
  #       description = "Whether to enable my custom service.";
  #     };

  #     key = mkOption {
  #       type = types.str;
  #       # default = "Hello, World!";
  #       description = "For services.syncthing.key";
  #     };
  #     cert = mkOption {
  #       type = types.str;
  #       # default = "Hello, World!";
  #       description = "For services.syncthing.cert";
  #     };
  #   };

  #   config = mkIf cfg.enable {
  # systemd.services.myService = {
  #   description = "My custom service";
  #   after = ["network.target"];

  #   serviceConfig = {
  #     ExecStart = "${pkgs.coreutils}/bin/echo ${cfg.message}";
  #   };

  #   wantedBy = ["multi-user.target"];
  # };
  services.syncthing = {
    enable = true;
    user = "nomada";
    dataDir = "/home/nomada/Sync"; # Default folder for new synced folders
    configDir = "/home/nomada/.config/syncthing"; # Folder for Syncthing's settings and keys
    # services.syncthing.key.text = secrets.nomada-t440s.syncthing.cert;
    # services.syncthing.cert.text = secrets.nomada-t440s.syncthing.cert;
    # key =
    # cert = ;
  };
  # };
}
