# Add your reusable NixOS modules to this directory, on their own file (https://nixos.wiki/wiki/Module).
# These should be stuff you would like to share with others, not your personal configurations.
{
  base = import ./base.nix;
  bootloader = import ./bootloader.nix;
  work = import ./work.nix;
  virtualisation = import ./virtualisation.nix;
  bluetooth = import ./bluetooth.nix;
  packages = import ./packages.nix;
  flatpak = import ./programs/flatpak.nix;
  i18n = import ./i18n.nix;
  network = import ./network.nix;
  fonts = import ./fonts.nix;
  shell = import ./shell.nix;
  k3s = import ./k3s.nix;
  gitops = import ./gitops.nix;

  desktop = {
    xserver = import ./desktop/xserver.nix;
    hyperland = import ./desktop/hyperland.nix;
    kiosk = import ./desktop/kiosk.nix;
  };

  services = {
    syncthing = import ./services/syncthing.nix;
    sound = import ./services/sound.nix;
    openssh = import ./services/openssh.nix;
    getty = import ./services/getty.nix;
  };

  users = {
    nomada = import ./users/nomada.nix;
    sudo = import ./users/sudo.nix;
  };
  programs = {
    zsh = import ./programs/zsh.nix;
    appimage = import ./programs/appimage.nix;
  };
  # TODO move to base
  maintenance = import ./maintenance.nix;
}
