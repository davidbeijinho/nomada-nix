{pkgs, ...}: {
  # age.secrets.spotify = {
  #   file = "${self}/secrets/spotify.age";
  #   owner = "mihai";
  #   group = "users";
  # };

  #   age.secrets.spotify = {
  #   file = "${self}/secrets/spotify.age";
  #   owner = "mihai";
  #   group = "users";
  # };

  # age = {
  #   # We're letting `agenix` know where the locations of the age files will be
  #   # in the server.
  #   secrets = {
  #     "id_ed25519" = {
  #       # file= "/secrets/secret1.age";
  #       file = ../../secrets/t440s.age;
  #       path = "/home/nomada/.ssh/id_ed25519";
  #       mode = "600";
  #       owner = "nomada";
  #       group = "users";
  #     };
  #     # emojiedDBCACert.file = "/root/secrets/emojiedDBCACert.age";
  #   };
  #   # Private key of the SSH key pair. This is the other pair of what was supplied
  #   # in `secrets.nix`.
  #   #
  #   # This tells `agenix` where to look for the private key.
  #   # identityPaths =   ["./id_ed25519"];
  #   #identityPaths = [
  #   #  "/home/coder/projects/nomada-nix"
  #   #  "/home/nomada/nomada-nix/id_ed25519"
  #   #];
  # };
  # age = {
  #   # We're letting `agenix` know where the locations of the age files will be
  #   # in the server.
  #   secrets = {
  #     # emojiedDBPassword.file = "./secrets/secret1.age";
  #     # emojiedDBCACert.file = "/root/secrets/emojiedDBCACert.age";
  #   };

  #   # Private key of the SSH key pair. This is the other pair of what was supplied
  #   # in `secrets.nix`.
  #   #
  #   # This tells `agenix` where to look for the private key.
  #   identityPaths = ["/root/.ssh/id_ed25519"];
  # };
  environment.systemPackages = with pkgs; [
    inputs.agenix.packages.x86_64-linux.default
  ];
}
