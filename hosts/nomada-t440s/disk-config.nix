{
  disko.devices = {
    disk = {
      vdb = {
        device = "/dev/disk/by-id/ata-INTEL_SSDSC2BF240A4L_CVDA418201TF2403GN";
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              type = "EF00";
              size = "500M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            root = {
              size = "100%";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/";
              };
            };
          };
        };
      };
    };
  };
}
