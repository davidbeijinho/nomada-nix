# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}:
#  let
#   secrets = import ../../secrets/nomada-secrets.nix;
#   # hostName = "nomada-t440s";
{
  # You can import other NixOS modules here
  imports = [
    inputs.home-manager.nixosModules.home-manager
    inputs.disko.nixosModules.disko
    inputs.agenix.nixosModules.default
    inputs.nix-flatpak.nixosModules.nix-flatpak

    # If you want to use modules your own flake exports (from modules/nixos):
    outputs.nixosModules.base
    outputs.nixosModules.maintenance
    outputs.nixosModules.bootloader
    outputs.nixosModules.i18n
    outputs.nixosModules.network
    outputs.nixosModules.packages
    outputs.nixosModules.flatpak
    outputs.nixosModules.fonts
    outputs.nixosModules.shell
    outputs.nixosModules.programs.zsh
    outputs.nixosModules.programs.appimage

    outputs.nixosModules.virtualisation
    outputs.nixosModules.bluetooth
    outputs.nixosModules.services.sound
    outputs.nixosModules.services.openssh
    outputs.nixosModules.services.syncthing
    # outputs.nixosModules.services.getty

    outputs.nixosModules.desktop.xserver
    outputs.nixosModules.desktop.hyperland

    outputs.nixosModules.users.nomada
    # Or modules from other flakes (such as nixos-hardware):
    # inputs.hardware.nixosModules.common-cpu-amd
    # inputs.hardware.nixosModules.common-ssd

    # You can also split up your configuration and import pieces of it here:
    # ./users.nix

    # Import your generated (nixos-generate-config) hardware configuration
    ./hardware-configuration.nix
    ./disk-config.nix
  ];

  # TODO move to options of network module
  # Define your hostname.
  networking.hostName = "nomada-t440s";

  # services.syncthing = {
  #   enable = true;
  #   key = secrets.nomada-t440s.syncthing.cert;
  #   cert = secrets.nomada-t440s.syncthing.cert;
  # };
  # services.syncthing = {
  # enable = true;
  # user = "nomada";
  # dataDir = "/home/nomada/Sync"; # Default folder for new synced folders
  # configDir = "/home/nomada/.config/syncthing"; # Folder for Syncthing's settings and keys
  # services.syncthing.key.text = secrets.nomada-t440s.syncthing.cert;
  # services.syncthing.cert.text = secrets.nomada-t440s.syncthing.cert;
  # key = "${./../../myconfig/key.pem}";
  # key = builtins.readFile ./../../myconfig/key.pem;
  # cert = "${./../../myconfig/cert.pem}";
  # cert = builtins.readFile ./../../myconfig/cert.pem;
  # key = "../../secrets/nomada-t440s.syncthing.key";
  # cert = "../../secrets/nomada-t440s.syncthing.cert";
  # key =
  # cert = ;
  # };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # TODO move this to a config/secret file
  # system.build.separateActivationScript = {
  system.activationScripts = {
    sshHandeling = {
      text = ''
        # $DRY_RUN_CMD echo "hello"
        $DRY_RUN_CMD mkdir -p /home/nomada/.ssh

        $DRY_RUN_CMD rm -f /home/nomada/.ssh/id_ed25519
        $DRY_RUN_CMD cp /etc/ssh/ssh_host_ed25519_key /home/nomada/.ssh/id_ed25519
        $DRY_RUN_CMD chown nomada:users /home/nomada/.ssh/id_ed25519

        $DRY_RUN_CMD rm -f /home/nomada/.ssh/id_ed25519.pub
        $DRY_RUN_CMD cp /etc/ssh/ssh_host_ed25519_key.pub /home/nomada/.ssh/id_ed25519.pub
        $DRY_RUN_CMD chown nomada:users /home/nomada/.ssh/id_ed25519.pub
      '';
      deps = [];
    };
  };

  home-manager = {
    backupFileExtension = "backup";
    extraSpecialArgs = {inherit inputs outputs;};
    users = {
      # Import your home-manager configuration
      nomada = import ../../home-manager/home-personal.nix;
    };
  };

  environment.systemPackages = [
    inputs.agenix.packages."x86_64-linux".default
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  system.stateVersion = "24.05"; # Did you read the comment?
}
