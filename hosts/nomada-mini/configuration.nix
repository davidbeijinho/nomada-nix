# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  # You can import other NixOS modules here
  imports = [
    inputs.home-manager.nixosModules.home-manager
    inputs.disko.nixosModules.disko
    # inputs.agenix.nixosModules.default
    # inputs.nix-flatpak.nixosModules.nix-flatpak

    # If you want to use modules your own flake exports (from modules/nixos):
    outputs.nixosModules.base
    outputs.nixosModules.maintenance
    outputs.nixosModules.bootloader
    outputs.nixosModules.i18n
    outputs.nixosModules.network
    # outputs.nixosModules.packages
    # outputs.nixosModules.flatpak
    outputs.nixosModules.fonts
    outputs.nixosModules.shell
    outputs.nixosModules.programs.zsh

    # outputs.nixosModules.virtualisation
    # outputs.nixosModules.bluetooth
    # outputs.nixosModules.services.sound
    outputs.nixosModules.services.openssh
    # outputs.nixosModules.services.syncthing
    # outputs.nixosModules.services.getty

    # outputs.nixosModules.desktop.xserver
    # outputs.nixosModules.desktop.kiosk
    # outputs.nixosModules.desktop.hyperland

    outputs.nixosModules.users.nomada
    outputs.nixosModules.users.sudo
    # outputs.nixosModules.k3s
    # outputs.nixosModules.gitops
    # Or modules from other flakes (such as nixos-hardware):
    # inputs.hardware.nixosModules.common-cpu-amd
    # inputs.hardware.nixosModules.common-ssd

    # You can also split up your configuration and import pieces of it here:
    # ./users.nix

    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./disk-config.nix
  ];

  # TODO move to options of network module
  # Define your hostname.
  networking.hostName = "nomada-mini"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #  wget
    devbox
    # git
    # firefox
    htop
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  home-manager = {
    extraSpecialArgs = {inherit inputs outputs;};
    users = {
      # Import your home-manager configuration
      nomada = import ../../home-manager/home-server.nix;
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
