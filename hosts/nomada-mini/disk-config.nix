{
  disko.devices = {
    disk = {
      vdb = {
        device = "/dev/disk/by-id/ata-SanDisk_SD6SB1M-256G-1006_152459400860";
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              type = "EF00";
              size = "500M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = ["umask=0077"];
              };
            };
            root = {
              end = "-3G";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/";
              };
            };
            plainSwap = {
              size = "100%";
              content = {
                type = "swap";
                discardPolicy = "both";
                resumeDevice = false; # do NOT resume from hiberation from this device
              };
            };
          };
        };
      };
    };
  };
}
