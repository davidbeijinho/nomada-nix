#!/usr/bin/env bash

# age -r age13y2sqtlhppv6hj26d9av0h86yhyly4e0l89chs8px0lj5cqxtqmqfj0l6k  -o ./secrets/nomada-secrets.nix.age ./secrets/nomada-secrets.nix
age -r age13y2sqtlhppv6hj26d9av0h86yhyly4e0l89chs8px0lj5cqxtqmqfj0l6k  -o ./secrets/nomada-secrets.nix.age ./secrets/nomada-decripted.nix
age -r age13y2sqtlhppv6hj26d9av0h86yhyly4e0l89chs8px0lj5cqxtqmqfj0l6k  -o ./secrets/nomada-t440s.syncthing.cert.age ./secrets/nomada-t440s.syncthing.cert
age -r age13y2sqtlhppv6hj26d9av0h86yhyly4e0l89chs8px0lj5cqxtqmqfj0l6k  -o ./secrets/nomada-t440s.syncthing.key.age ./secrets/nomada-t440s.syncthing.key
