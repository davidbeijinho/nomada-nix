#!/usr/bin/env bash

source ./scripts/decrypt.sh

mv ./secrets/nomada-decripted.nix ./secrets/nomada-secrets.nix

# sudo nixos-rebuild switch --flake .#nomada-t440s
sudo nixos-rebuild switch --flake .#


echo "{}" > ./secrets/nomada-secrets.nix
