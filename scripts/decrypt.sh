#!/usr/bin/env bash

# age -d -i key.txt -o ./secrets/nomada-secrets.nix ./secrets/nomada-secrets.nix.age
age -d -i key.txt -o ./secrets/nomada-decripted.nix ./secrets/nomada-secrets.nix.age
age -d -i key.txt -o ./secrets/nomada-t440s.syncthing.cert ./secrets/nomada-t440s.syncthing.cert.age
age -d -i key.txt -o ./secrets/nomada-t440s.syncthing.key ./secrets/nomada-t440s.syncthing.key.age
