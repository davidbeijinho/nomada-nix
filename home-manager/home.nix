# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  # You can import other home-manager modules here
  imports = [
    # If you want to use modules your own flake exports (from modules/home-manager):
    # outputs.homeManagerModules.example

    # Or modules exported from other flakes (such as nix-colors):
    # inputs.nix-colors.homeManagerModules.default

    # You can also split up your configuration and import pieces of it here:
    # ./nvim.nix
    # ./programs/git.nix
    # ./programs/firefox.nix
    ./programs/zsh.nix
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # Workaround for https://github.com/nix-community/home-manager/issues/2942
      allowUnfreePredicate = _: true;
    };
  };

  # This will add each flake input as a registry
  # To make nix3 commands consistent with your flake
  nix.registry = (lib.mapAttrs (_: flake: {inherit flake;})) ((lib.filterAttrs (_: lib.isType "flake")) inputs);

  # TODO: Set your username
  home = {
    username = "nomada";
    homeDirectory = "/home/nomada";
  };
  # programs.zsh.enable = true;

  #run ln -s $VERBOSE_ARG \
  #    ${builtins.toPath ./link-me-directly} $HOME
  # home.activation = {
  #  myActivationAction = lib.hm.dag.entryAfter ["writeBoundary"] ''
  #    $DRY_RUN_CMD /data/projects/dotfiles-nomada/install
  #  '';
  # };
  # home.activation = {
  #   myActivationAction = lib.hm.dag.entryAfter ["writeBoundary"] ''
  #     # $DRY_RUN_CMD echo "hello"
  #     $DRY_RUN_CMD mkdir -p /home/nomada/.ssh

  #     $DRY_RUN_CMD rm -f /home/nomada/.ssh/id_ed25519
  #     $DRY_RUN_CMD cp /etc/ssh/ssh_host_ed25519_key /home/nomada/.ssh/id_ed25519
  #     $DRY_RUN_CMD chown nomada:users /home/nomada/.ssh/id_ed25519

  #     $DRY_RUN_CMD rm -f /home/nomada/.ssh/id_ed25519.pub
  #     $DRY_RUN_CMD cp /etc/ssh/ssh_host_ed25519_key.pub /home/nomada/.ssh/id_ed25519.pub
  #     $DRY_RUN_CMD chown nomada:users /home/nomada/.ssh/id_ed25519.pub
  #   '';
  # };

  # ln -s $VERBOSE_ARG \
  #         ${builtins.toPath ./link-me-directly} $HOME
  # home.activation.myActivation =  lib.hm.dag.entryAfter ["writeBoundary"] ''
  #  touch ~/from-hm.txt
  # '';

  # builtins.trace config;
  # home.file.".ssh/id_ed25519" = {
  #  enable = true;
  #  source =  config.age.secrets.ssh_id_ed25519.path;
  # };
  # programs.ssh.enable = true;

  # home.file."id_ed25519.pub" = {
  #   enable = true;
  #   recursive = true;
  #   target = ".ssh/id_ed25519.pub";
  #   text ="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC9G7NXmDgi34UnEUalM8fpd6cKw23YEkuk5gIguBkEC";
  #   # text ="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOtzEm8rHI66SFWSTCD2tyds4iCkKA3Ri3FDMbRe0cCy";
  # };

  # home.file.".ssh/id_ed25519" = {
  #   enable = true;
  #   source = config.lib.file.mkOutOfStoreSymlink  "/etc/ssh/ssh_host_ed25519_key";

  # };

  # home.file.".ssh/id_ed25519.pub" = {
  #   enable = true;
  #   onChange = '' chown nomada:users /home/nomada/.ssh/id_ed25519.pub '';
  #   source = config.lib.file.mkOutOfStoreSymlink  "/etc/ssh/ssh_host_ed25519_key_pub";

  # };

  # home.file.firefox-profile = {
  #   # target = ".mozilla/firefox/nomada-firefox-profile";
  #   target = ".mozilla/firefox/nomada-firefox-profile-1";
  #   enable = true;
  #   recursive = true;
  #   source = config.lib.file.mkOutOfStoreSymlink "/data/Documents/firefox-profile";
  # };
  # home.file.firefox-profile-a = {
  #   # target = ".mozilla/firefox/nomada-firefox-profile";
  #   target = ".mozilla/firefox/nomada-firefox-profile-a";
  #   enable = true;
  #   recursive = true;
  #   source = config.lib.file.mkOutOfStoreSymlink "/data/Documents/firefox-profile";
  # };
  # home.file.firefox-profile-b = {
  #   # target = ".mozilla/firefox/nomada-firefox-profile";
  #   target = ".mozilla/firefox/nomada-firefox-profile-b";
  #   enable = true;
  #   recursive = true;
  #   source = config.lib.file.mkOutOfStoreSymlink "/data/Documents/firefox-profile";
  # };

  # xdg.userDirs.enable = true;
  #  xdg.userDirs.createDirectories = true;
  # xdg.userDirs.documents = "/data/Documents";

  #  xdg.userDirs = {
  #     enable = true;
  #     createDirectories = false;
  #     # desktop = "${config.home.homeDirectory}/desktop";
  #     documents = "/data/Documents";
  #     # download = "${config.home.homeDirectory}/dl";
  #     # music = "${config.home.homeDirectory}/music";
  #     # pictures = "${config.home.homeDirectory}/pics";
  #     # publicShare = "${config.home.homeDirectory}/public";
  #     # templates = "${config.home.homeDirectory}/templates";
  #     # videos = "${config.home.homeDirectory}/videos";

  #     # extraConfig = ''
  #     #   {
  #     #     XDG_PROJECTS_DIR = "${config.home.homeDirectory}/projects";
  #     #     XDG_GAMES_DIR = "${config.home.homeDirectory}/games";
  #     #   }
  #     # '';
  #   };

  # programs.ssh.userKnownHostsFile "github.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl gitlab.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf"

  # home.file.banana = {
  #   enable = true;
  #   target = "/data/coisas";
  #   source = "/data/peras";
  # };

  #   home.file."${config.xdg.configHome}" = {
  #   source = ../../dotfiles;
  #   recursive = true;
  # };

  # home.file."dotfiles" = {
  #   target = "fake/..";
  #   source = ../../../../dotfiles;
  #   recursive = true;
  # };

  # Add stuff for your user as you see fit:
  # programs.neovim.enable = true;
  # home.packages = with pkgs; [ steam ];

  # Enable home-manager and git
  programs.home-manager.enable = true;
  programs.git.enable = true;

  # Nicely reload system units when changing configs
  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "24.05";
}
