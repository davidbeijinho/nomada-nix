# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  # You can import other home-manager modules here
  imports = [
    ./home.nix
    ./programs/git-work.nix
    ./programs/neovim.nix
    ./programs/ssh-work.nix
    ./programs/firefox-work.nix
    ./programs/zsh-work.nix
  ];
}
