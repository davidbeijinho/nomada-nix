# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  # You can import other home-manager modules here
  imports = [
    ./home.nix
    ./programs/git-personal.nix
    # ./programs/firefox-personal.nix
    # ./programs/zsh-personal.nix
  ];

  home.file.".xinitrc" = {
    enable = true;
    text = "xscreensaver & exec firefox -width 1366 -height 768";
  };

  home.file.".xscreensaver" = {
    enable = true;
    text = ''
      mode: blank
      lock: False
      fade: False
      unfade: False
      dpmsQuickOff: True
      timeout: 0:01:00
      dpmsStandby: 0:01:00
      dpmsSuspend: 0:01:00
      dpmsOff: 1:00:00
      dpmsEnabled: True
    '';
  };
}
