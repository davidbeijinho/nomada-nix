{config, ...}: {
  programs.git = {
    enable = true;
    userName = "David Beijinho";
    includes = [
      {
        condition = "gitdir:/data/projects/WORK/";
        contents = {
          user = {
            # TODO move this to some config
            email = "d.beijinho@laserhub.com";
          };
        };
      }
      {
        condition = "gitdir:/data/projects/PERSONAL/";
        contents = {
          user = {
            # TODO move this to some config
            email = "davidbeijinho@gmail.com";
          };
        };
      }
    ];
  };
  # programs.ssh.enable=true;
  # # TODO i only want this for work machine
  programs.ssh.matchBlocks = {
    "gitlab.personal" = {
      hostname = "gitlab.com";
      # user = "davidbeijinho";
      # TODO would be nice if this file come from config
      identityFile = "/home/nomada/.ssh/gitlabPersonal";
      # identitiesOnly= true;
    };
  };
}
