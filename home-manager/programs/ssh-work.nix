{...}: {
  # programs.git = {
  #   enable = true;
  #   userName = "David Beijinho";
  #   userEmail = "d.beijinho@laserhub.com";
  # };
  programs.ssh.enable = true;
  # TODO i only want this for work machine
  programs.ssh.matchBlocks = {
    "gitlab.personal" = {
      hostname = "gitlab.com";
      # user = "davidbeijinho";
      identityFile = "/home/nomada/.ssh/gitlabPersonal";
      # identitiesOnly= true;
    };
  };
}
