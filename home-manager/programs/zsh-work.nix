{
  programs.zsh = {
    shellAliases = let
      # lh_checkout="~/Projects/lh-scripts/lh_checkout.sh"
      # sdo() sudo zsh -c "$functions[$1]" "$@"
      lhdevpath = "/data/projects/WORK/lh-develop";
      runner = "podman";
      # runner="docker"

      # for podman
      server_container_name = "lh-develop_lh-server_1";
      # for docker
      # server_container_name=lh-develop-lh-server-1

      execOnServerContainer = "${runner} exec -e NODE_ENV=development ${server_container_name}";
      npmOn = "npm --prefix ${lhdevpath}";
      npmOnClient = "${npmOn}/client";
      npmOnToolkit = "${npmOn}/lh-ui-toolkit";
    in {
      "lhshell.s" = "${runner} exec -i -t ${server_container_name} /bin/bash";
      "lhshell.w" = "${runner} exec -i -t lh-develop_lh-worker_1 /bin/bash";

      lhexec = "${runner} exec -t ${server_container_name}";

      lhdown = "${runner}-compose down";

      lhstop = "
        lhdown
        rm -fr node_modules
        rm -fr ./client/node_modules
        rm -fr ./server/node_modules
        rm -fr ./lh-ui-toolkit/node_modules
      ";

      lhclean = "
        lhstop
        ${runner} volume rm lh-develop_mongo-data
        ${runner} volume rm lh-develop_db-data
        ${runner} volume rm lh-develop_lh-worker
        ${runner} volume rm lh-develop_lh-server
      ";

      lhstart = "lhclean lhup";

      lhinstall = "${execOnServerContainer} npm i";

      "lhui.install" = "${npmOnToolkit} install";

      "lhclient.install" = "${npmOnClient} install";

      "storybook.run" = "${npmOnClient} run storybook";

      "storybook.dev" = "
        lhfront.install
        storybook.run";

      "lhlint.ts.dev" = "${npmOnClient} run lint:tsc";
      "lhlint.client.run" = "${npmOnClient} run lint:fix";
      "lhlint.lhui.run" = "${npmOnToolkit} run lint:fix";

      "lhlint.lhui.dev" = "
        lhui.install
        lhlint.lhui.run";

      "lhlint.client.dev" = "
        lhclient.install
        lhlint.client.run";

      "lhlint.all.dev" = "
        lhlint.lhui.dev
        lhlint.client.dev
        lhlint.ts.dev";

      "lhlint.all.run" = "
        lhlint.lhui.run
        lhlint.client.run";

      "lhfront.install" = "
        lhclient.install
        lhui.install";

      "lhfront.run" = "${npmOnClient} run start";
      "lhfront-sta.run" = "LH_SERVER_ADDRESS='https://staging.laserhub.com' lhfront.run";
      "lhfront-dev.run" = "LH_SERVER_ADDRESS='https://dev.laserhub.com' lhfront.run";
      "lhfront-slt.run" = "LH_SERVER_ADDRESS='https://mercury-sandbox.laserhub.com' lhfront.run";
      "lhfront-phoenix.run" = "LH_SERVER_ADDRESS='https://phoenix-temp-sandbox.laserhub.com' lhfront.run";

      "lhfront-dev.dev" = "
        lhfront.install
        lhfront-dev.run";

      "lhfront-slt.dev" = "
        lhfront.install
        lhfront-slt.run";

      "lhfront-phoenix.dev" = "
        lhfront.install
        lhfront-phoenix.run";

      # lhdev="
      # lhinstall &&
      # lhui.run &
      # lhcomponents.run
      # "

      # Lhdev() {
      #     lhcomponents.dev
      #     lhui.dev
      # }

      lhdev = "
        {
            lhcomponents.dev
        } &
        {
            lhui.dev
        } &
      ";

      # Lhdevkill() {
      #     ps -ef | grep "styleguid" | grep -v grep | awk '{print $2}' | xargs kill -9
      # }

      # lhdevkill='Lhdevkill';

      # lhup="
      #     lhstop
      #     lhinstall
      #     ${runner}-compose up --build
      # ";

      "lhup.build" = "lhstop ${runner}-compose up --build";

      lhup = "lhstop ${runner}-compose up";

      "lhmigrate.up" = "lhmigrate.mysql.up lhmigrate.mongo.up";

      "lhmigrate.down" = "lhmigrate.mysql.down lhmigrate.mongo.down";
      "lhmigrate.mysql.up" = "${execOnServerContainer} npx knex --knexfile ./knexfile.sample.js migrate:latest";
      "lhmigrate.mysql.down" = "${execOnServerContainer} npx knex --knexfile ./knexfile.sample.js migrate:rollback";
      "lhmigrate.mongo.up" = "${execOnServerContainer} npx migrate-mongo up";
      "lhmigrate.mongo.down" = "${execOnServerContainer} npx migrate-mongo down";
      # ${runner} exec -i -t lh-develop_mysql_1  mysql --user=root --password=123123  --execute=\"ALTER USER 'laserhub'@'%' IDENTIFIED WITH mysql_native_password BY '123123';\"
      #    ${runner} exec -i -t lh-develop_mysql_1  mysql --user=\"root\" --password=\"123123\"  --execute=\"ALTER USER 'laserhub'@'%' IDENTIFIED WITH mysql_native_password BY '123123';\"
      lhseed = "${execOnServerContainer} npx knex --knexfile ./knexfile.sample.js seed:run";
      # lhmigrate

      # VolumePrune = "${runner} volume prune";
    };
  };
}
