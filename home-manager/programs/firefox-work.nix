{
  config,
  pkgs,
  ...
}: {
  programs.firefox = {
    enable = true;
    # profiles = {
    #   default = {
    #     id = 0;
    #     isDefault = true;
    #     name = "default";
    #     # path = "/data/Documents/firefox-profile";
    #   };
    #   nomada = {
    #     id = 1;
    #     isDefault = false;
    #     name = "nomada";
    #     path = "nomada-firefox-profile";
    #   };
    #   nomada-a = {
    #     id = 2;
    #     isDefault = false;
    #     name = "nomada-a";
    #     path = "nomada-firefox-profile-a";
    #   };
    # };
  };

  # profiles = flip mapAttrs' cfg.profiles (_: profile:
  #   nameValuePair "Profile${toString profile.id}" {
  #     Name = profile.name;
  #     Path = if isDarwin then "Profiles/${profile.path}" else profile.path;
  #     IsRelative = 1;
  #     Default = if profile.isDefault then 1 else 0;
  #   }) // {
  #     General = { StartWithLastProfile = 1; };
  #   };

  # profilesIni =

  # [Profile0]
  # Name=nomada
  # IsRelative=0
  # Path=/data/Documents/firefox-profile
  # Default=1

  # [General]
  # StartWithLastProfile=1
  # Version=2

  home.file.firefoxProfileIni = {
    target = ".mozilla/firefox/profiles.ini";
    text = pkgs.lib.generators.toINI {} {
      Profile0 = {
        Name = "default";
        IsRelative = 0;
        Path = "/data/Documents/firefox-profile-personal";
        Default = 1;
      };

      Profile1 = {
        Name = "dev-edition-default";
        IsRelative = 0;
        Path = "/data/Documents/firefox-profile-dev-work";
      };

      General = {
        StartWithLastProfile = 1;
        Version = 2;
      };
    };
  };
  #  "${firefoxConfigPath}/profiles.ini" =
  #         mkIf (cfg.profiles != { }) { text = profilesIni; };

  # home.file.firefoxProfileLink = {
  #   target = ".mozilla/firefox/nomada-firefox-profile";
  #   enable = true;
  #   recursive= true;
  #   source = config.lib.file.mkOutOfStoreSymlink "/data/Documents/firefox-profile";
  # };
}
