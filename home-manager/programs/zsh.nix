{
  config,
  pkgs,
  ...
}: {
  programs.zsh = {
    enable = true;

    #   enableCompletion = true;
    #   # autosuggestion.enable = true;
    #   autosuggestion.enable = true;
    #   # syntaxHighlighting.enable = true;

    #   shellAliases = {
    #     ll = "ls -l";
    #     # update = "sudo nixos-rebuild switch";
    #   };
    #   history.size = 10000;
    #   history.path = "${config.xdg.dataHome}/zsh/history";

    #   # Your zsh config
    zplug = {
      enable = true;
      plugins = [
        # git
        # zsh-autosuggestions
        # {
        # name = "powerlevel10k";
        # src = pkgs.zsh-powerlevel10k;
        # file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
        # }

        # { name = "zsh-users/git"; } # Simple plugin installation
        {name = "zsh-users/zsh-autosuggestions";} # Simple plugin installation
        # { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; } # Installations with additional options. For the list of options, please refer to Zplug README.
      ];
    };
    plugins = [
      {
        name = "powerlevel10k";
        src = pkgs.zsh-powerlevel10k;
        file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      }
      {
        name = "powerlevel10k-config";
        src = ./p10k-config;
        file = "p10k.zsh";
      }
    ];

    oh-my-zsh = {
      enable = true;
      plugins = [
        "git"
        #  "zsh-autosuggestions"
      ];
      #     # theme = "robbyrussell";
      # theme = "powerlevel10k/powerlevel10k";
    };

    initExtra = "POWERLEVEL9K_DISABLE_CONFIGURATION_WIZARD=true";
    # initExtra = ''
    #     [[ ! -f ${./p10k.zsh;} ]] || source ${./p10k.zsh}
    #   '';
  };
}
