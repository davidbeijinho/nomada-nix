{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  programs.zsh = {
    shellAliases = let
      runner = "podman";
      # runner="docker"
    in {
      #  export PATH="$PATH:$HOME/bin:/data/DataBin"

      k9s = "${runner} run --rm -it -v /home/nomada/.kube/config:/root/.kube/config quay.io/derailed/k9s";
      k9s-mini = "${runner} run --rm -it -v /home/nomada/.kube/mini-nomada:/root/.kube/config quay.io/derailed/k9s";
      k9s-maxi = "${runner} run --rm -it -v /home/nomada/.kube/maxi-nomada:/root/.kube/config quay.io/derailed/k9s";
      k9s-panda = "${runner} run --rm -it -v /home/nomada/.kube/panda-nomada:/root/.kube/config quay.io/derailed/k9s";
      k9s-contabo = "${runner} run --rm -it -v /home/nomada/.kube/contabo-nomada:/root/.kube/config quay.io/derailed/k9s";
      #  k9s-mood="${runner} run --rm -it -v /home/nomada/.kube/mood-nomada:/root/.kube/config quay.io/derailed/k9s"
      #  k9s-urso="${runner} run --rm -it -v /home/nomada/.kube/urso-nomada:/root/.kube/config quay.io/derailed/k9s"

      # TODO fix this
      # export KUBECONFIG=~/.kube/config:~/.kube/mini-nomada:~/.kube/panda-nomada:~/.kube/contabo-nomada:~/.kube/maxi-nomada

      # ~/.kube/mood-nomada:
      #  screenDocked="wlr-randr --output DP-4 --mode 3440x1440 px, 75.050003 Hz --pos 0,0 && wlr-randr --output eDP-1 --pos 3445,360"
      #  screenDocked1="wlr-randr --output DP-5 --mode 3440x1440 px, 75.050003 Hz --pos 0,0 && wlr-randr --output eDP-1 --pos 3445,360"

      "brain-update" = "git -C /home/nomada/Sync/brain-nomada stash --all  && git -C /home/nomada/Sync/brain-nomada pull && git -C /home/nomada/Sync/brain-nomada stash apply";
    };
    # sessionVariables = {
    #   KUBECONFIG="~/.kube/config:~/.kube/mini-nomada:~/.kube/panda-nomada:~/.kube/contabo-nomada:~/.kube/maxi-nomada";
    # };
  };
  # environment.variables = {}
  # home.file.".kube" = {
  #   enable = true;
  #   source = config.lib.file.mkOutOfStoreSymlink "/data/kube";
  # };
}
