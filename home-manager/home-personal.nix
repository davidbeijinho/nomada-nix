# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: let
  secrets = import ../secrets/nomada-secrets.nix;
in {
  # You can import other home-manager modules here
  imports = [
    ./home.nix
    ./programs/git-personal.nix
    ./programs/firefox-personal.nix
    ./programs/zsh-personal.nix
  ];

  # home.file.".config/syncthing/key.pem" = {
  #   enable = true;
  #   # text = builtins.readFile ./../../myconfig/key.pem;
  #   # text = builtins.readFile ./../../secrets/nomada-t440s.syncthing.cert;
  #   # source = ../../secrets/nomada-t440s.syncthing.cert.age;
  #   text = secrets.nomada-t440s.syncthing.key;
  #   # source = "./../../myconfig/key.pem";
  # };
  # home.file.".config/syncthing/cert.pem" = {
  #   enable = true;
  #   # text = builtins.readFile ./../../myconfig/key.pem;
  #   # text = builtins.readFile ./../../secrets/nomada-t440s.syncthing.cert;
  #   # source = ../../secrets/nomada-t440s.syncthing.cert.age;
  #   text = secrets.nomada-t440s.syncthing.cert;
  #   # source = "./../../myconfig/key.pem";60de
  # };

  # TODO move this to a module
  home.file.".kube" = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/kube";
  };

  home.file.Desktop = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Desktop";
  };
  home.file.Documents = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Documents";
  };
  home.file.Downloads = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Downloads";
  };
  home.file.Music = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Music";
  };
  home.file.Pictures = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Pictures";
  };
  home.file.Templates = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Templates";
  };
  home.file.Videos = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Videos";
  };
  home.file.Public = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Public";
  };

  home.file.projects = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/projects";
  };
  home.file.Sync = {
    enable = true;
    source = config.lib.file.mkOutOfStoreSymlink "/data/Sync";
  };
}
